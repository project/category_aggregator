<?php

/**
 * This is RSS 2.0 parsing callback function
 */
function _category_aggregator_RSS20_parse($feed_XML, $feed)
{
	foreach ($feed_XML->xpath('//item') AS $news)
	{
		// for PHP > 5.1.2 get 'content' namespace
		$content = (array)$news->children('content');

		$news = (array)$news;

		if ($news['guid'])
			$guid = $news['guid'];
		else
			$guid = NULL;

		if ((string)$news['title'])
			$title = (string)$news['title'];
		else
			$title = '';

		if ((string)$news['description'])
			$body = (string)$news['description'];
		// some sources use content:encoded as description i.e. PostNuke PageSetter module
		elseif ((string)$news['encoded'])  //content:encoded for PHP < 5.1.2
			$body = (string)$news['encoded'];
		elseif ((string)$content['encoded'])  //content:encoded for PHP >= 5.1.2
			$body = (string)$content['encoded'];
		else
			$body = $news['title'];

		$teaser = node_teaser($body);

		if ($feed_XML->channel->title)
			$original_author = (string)$feed_XML->channel->title;
		else
			$original_author = $feed->original_author;

		if ($news['link'])
			$original_url = $news['link'];
		else
			$original_url = NULL;

		$timestamp = strtotime($news['pubDate']);
		if ($timestamp === FALSE)
			$timestamp = time();

		$additional_taxonomies = array();

		if ((string)$news['category'] && !empty($news['category']))
		{
			if (is_array($news['category'])) $news['category'] = $news['category'][0];
			$additional_taxonomies['RSS Categories'] = explode('/', $news['category']);
		}

		_category_aggregator_add_item($title, $body, $teaser, $original_author, $feed, $additional_taxonomies, $timestamp, $original_url, $guid, array());
	}
}
