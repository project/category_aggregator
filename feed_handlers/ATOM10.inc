<?php

/**
 * This is ATOM 1.0 parsing callback function
 */
function _category_aggregator_ATOM10_parse($feed_XML, $feed)
{
	foreach ($feed_XML->entry AS $key => $news)
	{
		if ($news->id)
			$guid = (string)$news->id;
		else
			$guid = NULL;
			
		$additional_taxonomies = array();
			
		if ($news->category)
		{
			$additional_taxonomies['ATOM Categories'] = array();
			foreach ($news->category AS $category)
				$additional_taxonomies['ATOM Categories'][] = (string)$category['term'];
		}
			
		$title = (string)$news->title;
	  
		if ($news->content)
		    $body = (string)$news->content;
		else if ($news->summary)
		    $body = (string)$news->summary;
		
		if ($news->content['src'])
		  $original_url = (string)($news->content['src']);
			
		if ($news->summary)
			$teaser = (string)$news->summary;
		else
			$teaser = node_teaser($body);
		
		$author_found = FALSE;
			
		if ($news->source->author->name)
		{
      $original_author = (string)($news->source->author->name);
		  $author_found = TRUE;				
		}
		else if ($news->author->name)
		{
      $original_author = (string)($news->author->name);
		  $author_found = TRUE;
		}

		if ($feed_XML->author->name && !$author_found)
		  $original_author = (string)($feed_XML->author->name);
		else if (!$author_found)
			$original_author = $feed->original_author;
		
		if ($news->link['href'])
      $original_url = (string)($news->link['href']);
		else 
			$original_url = NULL;
			
		$timestamp = strtotime((string)($news->published));
		if ($timestamp === FALSE)
		  $timestamp = time();
			
		_category_aggregator_add_item($title, $body, $teaser, $original_author, $feed, $additional_taxonomies, $timestamp, $original_url, $guid, array());
	}
}