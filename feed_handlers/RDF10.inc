<?php

/**
 * This is RSS1.0/RDF basic parsing callback function
 */
function _category_aggregator_RDF10_parse($feed_XML, $feed)
{
	// set category splitter (space is for del.icio.us feed)
	$category_splitter = ' ';
	
	// get the default original author
	if ($feed_XML->channel->title)
		$oa = (string)$feed_XML->channel->title;
	else
		$oa = $feed->original_author;
		
	// get all namespaces
	if (version_compare(phpversion(), '5.1.2', '<'))
	{
		//versions prior 5.1.2 don't allow namespaces
		$namespaces['default'] = NULL;
	} 
	else
		$namespaces = $feed_XML->getNamespaces(TRUE);

	foreach ($feed_XML->item as $news)
	{
		//initialization
		$guid = $original_url = NULL;
        $title = $body = $teaser = '';
        
		$timestamp = time();
		$additional_taxonomies = array();
		$original_author = $oa;
		
		foreach($namespaces as $ns_prefix => $ns_link)
        {
			//get about attribute as guid
			foreach ($news->attributes($ns_link) as $name => $value)
				if ($name = 'about')
					$guid = $value;

			//get children for current namespace
			if (version_compare(phpversion(), '5.1.2', '<'))
				$ns = (array)$news;
			else
				$ns = (array)$news->children($ns_link);
			
			//title
			if ((string)$ns['title'])
				$title = (string)$ns['title'];
			
			//description or dc:description
			if ((string)$ns['description'] && $body <> '')
				$body = (string)$ns['description'];
			
			//link
			if ((string)$ns['link'])
				$original_url = (string)$ns['link'];
			
			//dc:creator
			if ((string)$ns['creator'])
				$original_author = (string)$ns['creator'];
			
			//dc:date
			if ((string)$ns['date'])
				$timestamp = strtotime((string)$ns['date']);
			
			//content:encoded
			if ((string)$ns['encoded'])
				$body = (string)$ns['encoded'];
				
			//dc:subject
			if ((string)$ns['subject'])
			{
				//there can be multiple category tags
				if (is_array($ns['subject']))
				{
					foreach ($ns['subject'] as $cat)
					{
						if (is_object($cat))
							$additional_taxonomies['RDF Categories'][] = trim(strip_tags($cat->asXML()));
						else
							$additional_taxonomies['RDF Categories'][] = $cat;
					}
				}
				else //or single tag
					$additional_taxonomies['RDF Categories'] = explode($category_splitter, (string)$ns['subject']);
			}
        }
        
        // description is not mandatory so use title if description not present
		if (!$body)
			$body = $title;
		
		//make teaser
		$teaser = node_teaser($body);
        
		// if there are no link tag but rdf:about is provided
		if (!$original_url && $guid)
			$original_url = $guid;
		_category_aggregator_add_item($title, $body, $teaser, $original_author, $feed, $additional_taxonomies, $timestamp, $original_url, $guid, array());
	}
}
